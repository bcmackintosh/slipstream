angular.module('theming', ['ngResource', 'ngRoute', 'ngMaterial'])
.config(function($routeProvider, $locationProvider, $mdThemingProvider) {
    // Configure Material Design
    $mdThemingProvider.theme('default')
      .primaryPalette('grey')
      .accentPalette('blue');

    // Configure routing
    $locationProvider.html5Mode(true);

    $routeProvider
    .when("/vis", {
      templateUrl: "views/vis"
      })
    .otherwise({redirectTo: "/"})
  }
)
.controller('toolbar', ['$scope', '$http', function($scope, $http) {
  $scope.parse = function() {
    $http.post('/file/parse')
      .success(function(data, status, headers, config) {
        $scope.files = data;
      })
      .error(function(err) {
        console.log(err);
      });
  }
}])
.controller('data', ['$scope', '$http', function($scope, $http) {
  $scope.lConst = 10;
  $scope.sConst = 10;
  $scope.eConst = 10;

  $scope.subscribe = function(table) {
    $http.post('/sub/' + table)
      .success(function(data, status, headers, config) {
        $scope.files = data.files;
        $scope.stats = data.stats;
      })
      .error(function(err) {
        console.log(err);
      });
  }

  $scope.subscribe('project');
}])
.directive('myDiv', function() {
  return {
    restrict: 'E',
    template: 'foo: {{test.foo}}'
  }
});
