var fs = require('fs');

var File = require('../models/bin/file.js');
var lang = require('../languages/node/node');

var ext = lang.FILE_ENDING;

/**
 * Returns a File object created from parsed data.
 *
 * @param  {data: Object} Object representing parsed file data.
 * @return {File} A File object
 */
var formatFile = (data) => {
  return new File(data.path, data.lines, data.comments, data.dependencies, data.fileExports, data.depAliases);
}

/**
 * Returns an emitter for parsing a file.  The function returned emits:
 *  'count': {
 *    'lines': Number,        Number of lines in the file
 *    'path': String,         Path to the file
 *    'dependencies': Array   An array of imported files used by the file
 *  }
 *
 * @param  {filepath: String}
 * @return {function} The emitter
 */
var processFile = filepath => {
  let count = 0;
  let comments = 0;
  let fileExports = [];
  let dependencies = [];
  let depAliases = [];

  if (!filepath.endsWith(ext)) {
    return;
  }

  let readStream = fs.createReadStream(filepath);

  readStream.on('data', (chunk) => {
    let data = lang.parseFile(chunk);
    dependencies = dependencies.concat(data.dependencies);
    fileExports = fileExports.concat(data.fileExports);
    depAliases = depAliases.concat(data.depAliases);
    count += data.lines;
    comments += data.comments;
  });

  readStream.on('error', (e) => {
    console.log('error with!\n', e);
  });

  readStream.on('end', () => {
    // Create a "File" object from the parsed data.
    let file = formatFile({
      'lines': count,
      'comments': comments,
      'path': filepath,
      'dependencies': dependencies,
      'fileExports': fileExports,
      'depAliases': depAliases
    });

    this.emit('file', file);
  });

  return readStream;
}

// Debug code to check that a file is being parsed correctly.
var projectReq = require('../config/project.json')
var projectPath = projectReq.path;

var filePath = projectPath + '/workers/file-parser.js';

var fileProcessor = processFile(filePath);

fileProcessor.on('file', file => {
  console.log(file);
});

module.exports = { 'processFile': processFile };
