var helpme = require('./lib/database');

var getAll = table => {
  return helpme.getAll(table);
};

var getChanges = table => {
  return helpme.getChanges(table);
};

module.exports = {
  'allFrom': getAll,
  'changesFrom': getChanges
}
