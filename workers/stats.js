var files = fileData => {
  console.log('processing file stats');
  let fileStats = {
    'lines': 0,
    'deps': 0,
    'calls': 0,
    'fileExports': 0
  }

  let tempDeps = [];

  fileData.forEach(file => {
    fileStats.lines += file.lines;
    fileStats.fileExports += file.fileExports.length;
    if (!!file.dependencies) {
      tempDeps = tempDeps.concat(file.dependencies);
    }
  });

  fileStats.deps = uniqueArray(tempDeps).length;

  console.log(JSON.stringify(fileStats, null, 2));
  return fileStats;
}

var uniqueArray = arrI => {
  var obj = {};
  var arrO = [];

  for(let i = 0; i < arrI.length; i++) {
    if (!obj.hasOwnProperty(arrI[i])) {
      arrO.push(arrI[i]);
      obj[arrI[i]] = 1;
    }
  }

  return arrO;
}

module.exports = {
  'files': files
}
