var helpme = require('./lib/database.js');
var traceur = require('traceur');

var flag = false;

const TABLE_NAME = 'project';

var storeObject = (object) => {
  switch(object.type) {
    case 'File':
      storeFile(object);
    break;
  }
}

var storeFile = file => {
  file.key = file.path;
  helpme.add(TABLE_NAME, file);
}

var clearAll = () => {
  helpme.clearTable(TABLE_NAME);
}

module.exports = {
  'clearAll': clearAll,
  'object': storeObject
}
