/**
 * Directory parser, which gives entry points into the parse configurations,
 * and a listener that emits file and directory data.
 */

var fs = require('fs');
var callbacks = require('when/callbacks');
var osPath = require('path');
var emitter = require('events').EventEmitter;

var fileParse = require('./file-parser');
var Directory = require('../models/bin/directory.js');

var listener = new emitter;

/**
 * Returns a call to the database to store parsed data from a given directory.
 *
 * @param  {path: String} Path to directory to be parsed in this directory.
 * @return {function} A call to the parser which stores to the database.
 */
var getDirStats = path => {
  return getSubDirStats(path);
}

/**
 * Parses a given directory and stores the parsed data in a database.
 *
 * @param  {path: String} Path to the directory to be parsed.
 * @return {null} Stored in a database instead.
 */
var getSubDirStats = path => {
  readDir(path)
    .then(result => {
      let err = result[0];
      let pathList = result[1];

      let directory = new Directory(path);

      // Loop through all items in the directory.
      for (let itemPath of pathList) {
        let item = osPath.join(path, itemPath);

        // Check if the item should be ignored.
        let isIgnored = ignore(item);
        if (isIgnored) { // If it is ignored:
          // Skip this item.
          continue;
        }

        if (fs.lstatSync(item).isDirectory()) { // If the item is a directory:
          // Emit the directory, and then skip file parsing.
          listener.emit('directory', item);
          continue;
        };

        let parseListener = fileParse.processFile(item);

        if (!!parseListener) { // If the parse listener exists:
          // Emit the file data.
          parseListener.on('file', file => {
            listener.emit('file', file);
          });
        }
      }

      // When the loop of directory items is done, let the listener know.
      listener.emit('done', 'done');
    });
};

/**
 * Returns a boolean based on if the path should be ignored.
 * @param  {path: String} Path of the item to check
 * @return {Boolean} Boolean based on if the item should be ignored.
 */
var ignore = path => {
  let ignored = getIgnored();

  for (let ignorePath of ignored) {
    if (path.contains(ignorePath)) {
      return true;
    }
  }

  return false;
}

/**
 * Returns an array of partial paths to ignore.
 * @return {Array} An array of partial paths.
 */
var getIgnored = () => {
  // TODO: Make a real ignored handler, instead of fudged hardcode.
  return ['.gitignore', '.git', 'node_modules', 'rethinkdb_data', 'upload'];
}

/**
 * Wrapper to return a lifted asynchronous directory read.
 *
 * @param  {path: String} Path to the directory.
 * @return {Array} [Error, Array of paths from the directory]
 */
var readDir = (path) => {
  return callbacks.lift(fs.readdir)(path);
}

module.exports = {
  'dirStats': getDirStats,
  'listener': listener
}
