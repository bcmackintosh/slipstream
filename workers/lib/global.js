// A singleton to track global states, be very careful making edits to this file!
var dbase = require('./database');

// "^" = project level directory.  e.g. "/opt/slipstream"

/**
 * Database configuration object.  Set by "^/config/database.json"
 * @type {JSON Object} Configuration to start the database properly
 */
global.database = require('../../config/database.json');

/**
 * Object representing the RethinkDB connection.  Set by "./database.js"
 * @type {JSON Object} RethinkDB connection
 */
global.connection = null;

/**
 * Database connection listener.  Set by "./database.js"
 * @type {Event Emitter} Emits status of the database
 */
global.db = dbase.dbListener;

/**
 * Tables to insert into the database.
 * @type {Array} Tables to insert.
 */
global.tables = [
  'project'
];

/**
 * Config object for the project install info.  Set by "^/config/project.json"
 * @type {JSON Object} Configuration to set absolute paths for the project.
 */
global.project = require('../../config/project.json');
