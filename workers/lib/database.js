var r = require('rethinkdb');
var Emitter = require('events').EventEmitter;
var traceur = require('traceur');
var spawn = require('child_process').spawn;

const SERVER_READY = 'Server ready';
const SERVER_ALREADY_STARTED = 'is already in use, perhaps another instance of rethinkdb is using it.'
const ALREADY_EXISTS = 'already exists';

var dbListener = new Emitter;

dbListener.on('connected', cn => {
	connection = cn;
});

dbListener.on('error', err => {
  console.log('error:', err)
})

var spawnDB = spawn('rethinkdb');



var restartServer = () => {
  var spawnAux = spawn('ps', ['aux']);

  spawnAux.stdout.on('data', data => {
    let lines = data.toString().split('\n');
    let pids = [];
    let toKill = null;

    // Get PIDs
    for (let line of lines) {
      if (line.contains('rethinkdb')) {
        let pInfo = line.split(/[ ]+/);
        pids.push(pInfo[1]);
      }
    }

    // Find the first rethinkdb PID
    for (let pid of pids) {
      if (toKill == null || toKill > pid) {
        toKill = pid;
      }
    }

    spawn('kill', ['-9', toKill]);
    spawnDB = spawn('rethinkdb');
  });

  spawnAux.stderr.on('data', err => {
    console.log('error:', err)
  });
}

var initTables = (conn, tableList) => {
  for (let table of tableList) {
    r.db(database.db).tableCreate(table, {'primaryKey': 'path'}).run(conn, (err, success) => {
      if (!!err && err.message.contains(ALREADY_EXISTS)) {
        if (!checkTable(table, conn)) {
          console.log('Table not found:', table);
        }
      } else if (!!err && !err.message.contains(ALREADY_EXISTS)) {
        throw err;
      }
    });
  }

  dbListener.emit('table-init', true);
};

var checkTable = (table, conn) => {
  return r.db(database.db).tableList().run(conn, (err, result) => {
    if(result.findIndex(i => i == table) == -1) {
      return false;
    }
    return true;
  });
};


dbListener.on('restart', message => {
  console.log('Unable to reconnect, restarting server.')
  restartServer();
})


var connect = (tableList, emitter) => {
  r.connect(database, (err, conn) => {
    if (!!err) {
      emitter.emit('restart', 'Restart Needed');
    } else {
      r.dbCreate(database.db).run(conn)
        .then(dbConn => {
          initTables(dbConn, tableList);
        })
        .catch(err => {
          if (!!err && err.message.contains(ALREADY_EXISTS)) {
            initTables(conn, tableList);
          } else {
            throw err;
          }
        })
        .then(() => {
          emitter.emit('connected', conn);
        });
    }
  });
};

spawnDB.stdout.on('data', data => {
  let serverIsReady = data.toString().contains(SERVER_READY);

  if (serverIsReady) {
    initDb();
  }
});

spawnDB.stderr.on('data', data => {
  let err = data.toString();

  let serverRunning = err.contains(SERVER_ALREADY_STARTED);
  if (serverRunning) {
    console.log('Database server already started, attempting to reconnect.');
    initDb();
  } else {
    restartServer();
  }
});

var initDb = () => {
  connect(tables, dbListener);
}

var reconnect = () => {
	throw new Error('reconnect() is not implemented.');
}

var use = (query) => {
	let emitter = new Emitter;

	dbListener.on('connected', conn => {
		emitter.emit('data', query.run(conn));
	});

	return emitter;
};

var add = (table, data) => {
  r.table(table).insert(data).run(connection)
    .then(result => {
      if (result.first_error) {
        throw result.first_error;
      }
    })
    .catch(err => {
      replace(table, data)
    });
};

var replace = (table, data) => {
  r.table(table).get(data.key).replace(data).run(connection)
    .then(result => {
      if (result.first_error) {
        throw result.first_error;
      }
    });
};

var getAll = table => {
  return r.table(table).run(connection)
    .then(cursor => {
      return cursor;
    });
};

var getChanges = table => {
  return r.table(table).changes().run(connection)
    .then(cursor => {
      return cursor;
    });
}

var clearTable = table => {
  console.log('clearing table from database')
  r.db(database.db).tableDrop(table).run(connection)
    .then(result => {
      console.log('result:', result);
      initTables(connection, [table]);
    });
}

module.exports = {
  'add': add,
  'clearTable': clearTable,
	'dbListener': dbListener,
  'getAll': getAll,
  'getChanges': getChanges,
	'use': use
};
