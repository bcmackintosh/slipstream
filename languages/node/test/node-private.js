var chai = require('chai');
var nodeLang = require('../node');
var parse = nodeLang.parseFile;

chai.should();

describe('parseFile()', () => {
  describe('dependency pattern: "var <alias> = <require>"', () => {
    var testAlias = new Buffer('var foo = require("./bar");\n var bar = require(\'baz\');\n');
    var alias = parse(testAlias);

    it('should contain the appropriate for internal and extenal dependencies.', () => {
      alias.dependencies.should.include.members(['./bar', 'baz']);
    });

    it('should contain the appropriate alias and dependency for internal dependencies.', () => {
      alias.depAliases[0].name.should.equal('foo');
      alias.depAliases[0].dependency.should.equal('./bar');
    });

    it('should contain the appropriate alias and dependency for external dependencies.', () => {
      alias.depAliases[1].name.should.equal('bar');
      alias.depAliases[1].dependency.should.equal('baz');
    });
  });

  describe('export pattern: "module.exports = { <name1>: <value1>, <name2>: <value2>, ... }"', () => {
    var testAlias = new Buffer('module.exports = { foo: foo, \'bar\': bar, "baz": baz };\n');
    var alias = parse(testAlias);

    it('should contain all exported items.', () => {
      alias.fileExports.should.include.members(['foo', 'bar', 'baz']);
    });
  });

  describe('export pattern: "exports.<name> = <value>"', () => {
    var testAlias = new Buffer('exports.foo = foo;\n');
    var alias = parse(testAlias);

    it('should contain the exported item.', () => {
      alias.fileExports.should.include('foo');
    });
  });

  describe('in-line comments', () => {
    var testAlias = new Buffer('// Foo Bar // foo\n\n// foo \n // foo');
    var alias = parse(testAlias);

    it('should properly count in-line comment lines.', () => {
      alias.comments.should.equal(3);
    });
  });

  describe('block comments', () => {
    var testAlias = new Buffer('/* foo */ /* foo */ \n /* foo */ \n /* \n foo \n */ \n /* foo \n \n \n */');
    var alias = parse(testAlias);

    it('should properly count block comment lines.', () => {
      alias.comments.should.equal(9);
    });
  });

  describe('dependency calls', () => {
    var testAlias = new Buffer('var foo = require(\'./bar\');\n var baz = foo.bazinga;\n var baz = foo(baz);\n')
    var alias = parse(testAlias);
    var checkAlias = alias.depAliases[0];
    var calls = checkAlias.parsedCalls;

    it('should be named properly.', () => {
      checkAlias.name.should.equal('foo');
    });

    it('should be the proper dependency.', () => {
      checkAlias.dependency.should.equal('./bar');
    });

    it('should recognize "var <variable> = <alias>.<export>".', () => {
      calls.should.include('bazinga');
    });

    it('should recognize "var <variable> = <alias>()".', () => {
      calls.should.include('DIRECT_CALL');
    });
  })
});
