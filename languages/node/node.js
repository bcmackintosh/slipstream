/**
 * WARNING!
 *
 * Do not read this particular file, it is meant to be a basic library for parsing nodejs code.
 * It will kill kittens and make your mind bleed.  Your hands will turn to hooves, and a curse
 * of blindness will be spread through your family.
 *
 * You have been warned...
 */

const REQUIRE = 'require(';
const EXPORTS = 'exports';
const MODULE = 'module';
const FILE_ENDING = '.js';
const ALIAS_BRACKETS = ['[', '('];

/**
 * Returns an object based on the number of non-blank lines parsed from a file,
 * and the dependencies found in that file.  The object is structured like:
 *     { 'lines': <Number of Lines>,
 *       'dependencies': <An array of dependencies> }
 * @param  {chunk: Buffer} A read buffer of a file.
 * @return {Object} An object representing number of lines and dependencies.
 */
var parseFile = chunk => {
  let dependencies = [];
  let depAliases = [];
  let fileExports = [];
  let requireStep = REQUIRE.length + 1;


  let prev = 0;
  let lines = 0;
  let comments = 0;

  for(let curr = 0; curr < chunk.length; curr++) {
    // Since there are minimal dependencies per module, hopefully this won't hurt performance too badly.
    for(let iter in depAliases) {
      let alias = depAliases[iter];
      if (chunk[curr] == alias.name.charCodeAt(0)) {
        if (checkSyntaxWord(chunk, curr, alias.name + '.')) {
          curr += alias.name.length + 1;
          let depCall = getDepCall(chunk, curr);
          alias.parsedCalls.push(depCall);
          curr += depCall.length + 1;
        } else {
          for (let idx in ALIAS_BRACKETS) {
            if (checkSyntaxWord(chunk, curr, alias.name + ALIAS_BRACKETS[idx])) {
              alias.parsedCalls.push('DIRECT_CALL');
            }
          }
        }

      }
    }

    // If the line is a comment:
    if (checkComment(chunk, curr)) {
      comments++;
      curr = getEoL(chunk, curr) + 1;
      prev = curr;
    }

    if (checkCommentBlock(chunk, curr)) {
      let eocb = getEoCB(chunk, curr);
      comments += getLinesBetween(chunk, curr, eocb) + 1;
      let eol = getEoL(chunk, eocb) + 1;

      prev = eol;
      curr = eol;
    }

    // If the character is '\n' and the previous character was not '\n':
    if (chunk[curr] == '\n'.charCodeAt(0) && !shouldSkip(prev, curr)) {
      lines++;
      prev = curr;
    }

    // If the current character is 'r':
    if (chunk[curr] == 'r'.charCodeAt(0)) {
      // If the current index points at 'require('
      if (checkSyntaxWord(chunk, curr, REQUIRE)) {
        // Skip 'require(' and store the contained value in "dependencies."
        curr += requireStep;
        let dep = getDependency(chunk, curr)
        dependencies.push(dep);

        // Getting aliases requires the index to be at the beginning of the require statement.
        let alias = getAlias(chunk, curr - requireStep, dep);
        depAliases.push(alias);
      }
    }

    // If the current character is 'e':
    if (chunk[curr] == 'e'.charCodeAt(0)) {
      // If the current index points at 'exports'
      if (checkSyntaxWord(chunk, curr, EXPORTS)) {
        let exportVar = extractExport(chunk, curr);

        if (!!exportVar) {
          fileExports = fileExports.concat(exportVar);
        }
      }
    }
  }

  // Format the parsed data into an object.
  let data = {
    'lines': lines,
    'comments': comments,
    'dependencies': dependencies,
    'fileExports': fileExports,
    'depAliases': depAliases
  }

  return data;
}

var getAlias = (buffer, index, dependency) => {
  // TODO:
  // Cover the case of "<require>.export"
  let alias = {
    'name': '',
    'dependency': dependency,
    'parsedCalls': []
  }

  let eqIdx = index;
  let aliasStart = index;

  for (let i = index; i >= 0; i--) {
    // Find the position of '=' and store it.
    if (buffer[i] == '='.charCodeAt(0)) {
      eqIdx = i;
    }

    // If the current position is not equal to the position before the equals, the position of the equals is known,
    // and the current position is a break, parse the alias starting at this position.
    // Expecting the format:
    //     var <variable> = <require>
    //
    // This will find the start position of that variable to parse it in the next loop.
    if (eqIdx != index && i != eqIdx - 1 && buffer[i] == ' '.charCodeAt(0)) {
      aliasStart = i + 1;
      break;
    }
  }

  for (let i = aliasStart; i < eqIdx; i++) {
    if (buffer[i] != ' '.charCodeAt(0)) {
      alias.name += String.fromCharCode(buffer[i]);
    }
  }

  return alias;
}

var getDepCall = (buffer, index) => {
  let depCall = '';
  let endOfCallOpts = [';', ' '];

  for (let i = index; i < buffer.length; i++) {
    let character = String.fromCharCode(buffer[i]);

    // If the end of the call options does not contain the current character, add it to the depCall.
    if (endOfCallOpts.indexOf(character) == -1) {
      depCall += character;
    } else {
      break;
    }
  }

  return depCall;
}

/**
 * Returns an array of exported objects from the parsed file.
 * @param  {Buffer} buffer Read Stream
 * @param  {number} index  Index of current position in the Read Stream.
 * @return {Array}        Array of exported objects
 */
var extractExport = (buffer, index) => {
  // TODO: Handle the case "module.exports = <var>"
  if (isDirectExport(buffer, index)) {
    return getDirectExport(buffer, index);
  } else if (isExportObject(buffer, index)) {
    return getExportObject(buffer, index);
  }
}

/**
 * Returns a boolean based on if the chunk matches:
 *    exports.<var>
 * @param  {Buffer} buffer Read Stream
 * @param  {number} index  Index of current position in the Read Stream.
 * @return {Boolean}        True if it meets the requirements
 */
var isDirectExport = (buffer, index) => {
  let i = index + EXPORTS.length;

  if (buffer[i] == '.'.charCodeAt(0)) {
    return true;
  }

  return false;
}

/**
 * Returns a boolean based on if the chunk matches:
 *     module.exports = { name1: value1, name2: value2, ... }
 * @param  {Buffer} buffer Read Stream
 * @param  {number} index  Index of current position in the Read Stream.
 * @return {Boolean}       True if it meets the requirements
 */
var isExportObject = (buffer, index) => {
  let i = index - MODULE.length - 1;
  let eq = getAfterEquals(buffer, index);

  if (checkSyntaxWord(buffer, i, MODULE) && buffer[eq] == '{'.charCodeAt(0)) {
    return true;
  }

  return false;
}

/**
 * Returns the position of the first character which is not whitespace after an equals.
 * @param  {Buffer} buffer Read Stream
 * @param  {number} index  Index of current position in the Read Stream.
 * @return {number}       The position of the first character
 */
var getAfterEquals = (buffer, index) => {
  let eqCheck = false;

  for (let i = index; i < buffer.length; i++) {
    if (buffer[i] != ' '.charCodeAt(0) && eqCheck) {
      return i;
    }

    if (buffer[i] == '='.charCodeAt(0)) {
      eqCheck = true;
    }
  }
}

/**
 * Returns an array of exported objects from the file, based on the pattern:
 *     exports.<object>
 *
 * @param  {Buffer} buffer Read Stream
 * @param  {number} index  Index of current position in the Read Stream.
 * @return {Array}         Array of exported objects
 */
var getDirectExport = (buffer, index) => {
  let word = '';

  for (let i = EXPORTS.length + 1 + index; i < buffer.length; i++) {
    if (buffer[i] == ' '.charCodeAt(0) && buffer[i + 1] == '='.charCodeAt(0)) {
      return [word];
    }
    word += String.fromCharCode(buffer[i]);

  }
}

/**
 * Returns an array of exported objects from the file, based on the pattern:
 *     module.exports = { 'name': attribute }
 *
 * @param  {Buffer} buffer Read Stream
 * @param  {number} index  Index of current position in the Read Stream.
 * @return {Array}         Array of exported objects
 */
var getExportObject = (buffer, index) => {
  let exportList = [];
  let iter = index;
  let checkOpen = false;

  while (buffer[iter] != '}'.charCodeAt(0)) {
    if (buffer[iter] != '{'.charCodeAt(0) && buffer[iter] != ','.charCodeAt(0)) {
      iter++;
      continue;
    } else {
      checkOpen = true;
      iter++;
    }

    let attribute = '';

    for (let i = iter; i < buffer.length; i++) {
      if (buffer[i] == ':'.charCodeAt(0)) {
        exportList.push(attribute);
        iter = i;
        checkOpen = false;
        break;
      } else if (buffer[i] != ' '.charCodeAt(0) && buffer[i] != '\n'.charCodeAt(0) && buffer[i] != '\''.charCodeAt(0) && buffer[i] != '\"'.charCodeAt(0)) {
        attribute += String.fromCharCode(buffer[i]);
      }
    }

    iter++;
  }

  return exportList;
}

/**
 * Returns a boolean based on if the line begins with "//"
 * @param  {buffer: Buffer} File read buffer
 * @param  {index: Number} Current position of the file read buffer.
 * @return {Boolean} If the line is a comment.
 */
var checkComment = (buffer, index) => {
  if (buffer[index] == '/'.charCodeAt(0) && buffer[index + 1] == '/'.charCodeAt(0)) {
    return true;
  }

  return false;
}

/**
 * Returns a boolean based on if the line begins with "/*"
 * @param  {buffer: Buffer} File read buffer
 * @param  {index: Number} Current position of the file read buffer.
 * @return {Boolean} If the line is a comment.
 */
var checkCommentBlock = (buffer, index) => {
  if (buffer[index] == '/'.charCodeAt(0) && buffer[index + 1] == '*'.charCodeAt(0)) {
    return true;
  }

  return false;
}

var getLinesBetween = (buffer, index, endIndex) => {
  let newLines = 0;

  for (let i = index; i < endIndex + 1; i++) {
    if (buffer[i] == '\n'.charCodeAt(0)) {
      newLines++;
    }
  }
  return newLines;
}

/**
 * Returns the index at the next end of line in the Buffer.
 * @param  {buffer: Buffer} File read buffer.
 * @param  {index: Number} Current position of the buffer.
 * @return {Number} Index of the next character after the '\n'
 */
var getEoL = (buffer, index) => {
  for (let i = index; i < buffer.length; i++) {
    if (buffer[i] == '\n'.charCodeAt(0)) {
      return i;
    }
  }
}

/**
 * Returns the index at the next '*\/' in the Buffer.
 * @param  {buffer: Buffer} File read buffer.
 * @param  {index: Number} Current position of the buffer.
 * @return {Number} Index of the next character after the '\n'
 */
var getEoCB = (buffer, index) => {
  for (let i = index; i < buffer.length; i++) {
    if (buffer[i - 1] == '*'.charCodeAt(0) && buffer[i] == '/'.charCodeAt(0)) {
      return i;
    }
  }
}

/**
 * Checks if the buffer literally contains "require" at the given buffer index.
 * @param  {buffer: Buffer} File read Buffer
 * @param  {index: Number} Index of the guessed start of the require statement.
 * @return {Boolean} Returns if the buffer contains the "require" at the given index.
 */
var checkSyntaxWord = (buffer, index, syntaxWord) => {
  let word = '';
  let reqEnd = index + syntaxWord.length;

  for(let i = index; i < reqEnd; i++) {
    word += String.fromCharCode(buffer[i]);
  }

  return word == syntaxWord;
}

/**
 * Returns the string given in the "require()" statement.
 * @param  {buffer: Buffer} File read Buffer
 * @param  {index: Number} Index of the next item directly after "require(" in the buffer.
 * @return {String} String which is wrapped by the "require()" statement.
 */
var getDependency = (buffer, index) => {
  let word = '';
  let i = index;

  while(buffer[i + 3] != '\n'.charCodeAt(0)) {
    word += String.fromCharCode(buffer[i]);
    i++;
  }

  word = word.split('\')')[0];
  word = word.split('\")')[0];

  return word;
}

/**
 * Wrapper to check if the current value is one more than the previous value.  This is
 * used to clean up checking if two '\n' are next to eachother in a buffer.
 * @param  {prev: Number} The index of the previous '\n'
 * @param  {curr: Number} The index of the current '\n'
 * @return {Boolean} Checks if the current index -1 equals the previous index.
 */
var shouldSkip = (prev, curr) => {
  return curr - 1 == prev;
}

var _private = {
  'getAlias': getAlias,
  'getDepCall': getDepCall,
  'extractExport': extractExport,
  'isDirectExport': isDirectExport,
  'isExportObject': isExportObject,
  'getAfterEquals': getAfterEquals,
  'getDirectExport': getDirectExport,
  'getExportObject': getExportObject,
  'checkComment': checkComment,
  'checkCommentBlock': checkCommentBlock,
  'getLinesBetween': getLinesBetween,
  'getEoL': getEoL,
  'getEoCB': getEoCB,
  'checkSyntaxWord': checkSyntaxWord,
  'getDependency': getDependency,
  'shouldSkip': shouldSkip
}

module.exports = {
  'parseFile': parseFile,
  'FILE_ENDING': FILE_ENDING,
  '_private': _private
};

