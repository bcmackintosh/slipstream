var traceur = require('traceur');

var getName = path => {
  if (path.contains('/')) {
    var pathParts = path.split('/');
    var lastIndex = pathParts.length - 1;

    return pathParts[lastIndex];
  } else {

    return path;
  }
}

var getDeps = deps => {
  var depObj = {
    'internal': [],
    'external': [],
    'all': deps
  }

  deps.forEach(dep => {
    if (dep.contains('./')) {
      depObj.internal.push(dep);
    } else {
      depObj.external.push(dep);
    };
  });

  return depObj;
}

module.exports = {
  'getName': getName,
  'getDeps': getDeps
}
