var fs = require('fs');
var traceur = require('traceur');
var chai = require('chai');

var should = chai.should();

// TODO: Handle isFile check asynchronously.
var isFile = path => {
  return fs.lstatSync(path).isFile();
}

// TODO: Handle isDirectory check asynchronously.
var isDirectory = path => {
  return fs.lstatSync(path).isDirectory();
}

var asArray = item => {
  item.constructor.should.equal(Array);
  return item;
}

// TODO: Convert this to proxy handler after proxies are put into classes.
var asFile = path => {
  if (!isFile(path)) {
    throw new TypeError('The path is not recognized as a file.')
  } else {
    return path;
  }
}

// TODO: Convert this to proxy handler after proxies are put into classes.
var asDirectory = path => {
  if (!isDirectory(path)) {
    throw new TypeError('The path is not recognized as a file.')
  } else {
    return path;
  }
}

module.exports = {
  'asFile': asFile,
  'asDirectory': asDirectory,
  'asArray': asArray
}
