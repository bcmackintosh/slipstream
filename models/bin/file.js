var fsHelpme = require('../lib/fs-helpme');
var validate = require('../lib/validate');
var File = (function() {
  "use strict";
  function File(path, lines, comments, dependencies, fileExports, depAliases) {
    this.path = this.validate(path);
    this.name = this.getName(path);
    this.lines = lines;
    this.comments = comments;
    this.fileExports = fileExports;
    this.depAliases = depAliases;
    this.type = 'File';
    this.setDeps(dependencies);
  }
  return ($traceurRuntime.createClass)(File, {
    validate: function(path) {
      return validate.asFile(path);
    },
    getName: function(path) {
      return fsHelpme.getName(path);
    },
    setDeps: function(deps) {
      var deps = fsHelpme.getDeps(deps);
      this.iDeps = deps.internal;
      this.eDeps = deps.external;
      this.dependencies = deps.all;
    }
  }, {});
}());
module.exports = File;
