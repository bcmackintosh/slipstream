var File = require('./file.js');
var fsHelpme = require('../lib/fs-helpme');
var validate = require('../lib/validate');
var Directory = (function() {
  "use strict";
  function Directory(path) {
    this.name = this.getName(path);
    this.path = this.validatePath(path);
  }
  return ($traceurRuntime.createClass)(Directory, {
    getName: function(path) {
      return fsHelpme.getName(path);
    },
    validatePath: function(path) {
      return validate.asDirectory(path);
    },
    validateArray: function(item) {
      return validate.asArray(item);
    },
    add: function(ioItem) {
      if (this.ioItem == undefined) {
        this.ioItem = [];
      } else {
        this.validateArray(this.ioItem);
      }
      if (ioItem.constructor === Array) {
        this.ioItem = this.ioItem.concat(ioItem);
      } else {
        this.ioItem.push(ioItem);
      }
    }
  }, {});
}());
module.exports = Directory;
