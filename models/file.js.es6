var fsHelpme = require('../lib/fs-helpme');
var validate = require('../lib/validate');

class File {
  constructor(path, lines, comments, dependencies, fileExports, depAliases) {
    // Set Values
    this.path = this.validate(path);
    this.name = this.getName(path);
    this.lines = lines;
    this.comments = comments;
    this.fileExports = fileExports;
    this.depAliases = depAliases;
    this.type = 'File';

    // Run Initializers
    this.setDeps(dependencies);
  }

  validate(path) {
    return validate.asFile(path);
  }

  getName(path) {
    return fsHelpme.getName(path);
  }

  setDeps(deps) {
    var deps = fsHelpme.getDeps(deps);

    this.iDeps = deps.internal;
    this.eDeps = deps.external;
    this.dependencies = deps.all;
  }
}

module.exports = File;
