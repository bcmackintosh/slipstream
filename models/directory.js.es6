var File = require('./file.js');
var fsHelpme = require('../lib/fs-helpme');
var validate = require('../lib/validate');

class Directory {
  constructor(path) {
    this.name = this.getName(path);
    this.path = this.validatePath(path);
  }

  getName(path) {
    return fsHelpme.getName(path);
  }

  validatePath(path) {
    return validate.asDirectory(path);
  }

  validateArray(item) {
    return validate.asArray(item);
  }

  add(ioItem) {
    if (this.ioItem == undefined) {
      this.ioItem = [];
    } else {
      this.validateArray(this.ioItem);
    }

    if (ioItem.constructor === Array) {
      this.ioItem = this.ioItem.concat(ioItem);
    } else {
      this.ioItem.push(ioItem);
    }
  }
}

module.exports = Directory;
