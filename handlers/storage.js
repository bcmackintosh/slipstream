var traceur = require('traceur');

var parse = require('../workers/dir-parser');
var store = require('../workers/storage');

var iter = 0;

var init = directory => {
  parse.dirStats(project.path);
}

var clear = () => {
  console.log('Clearing database.');
  store.clearAll();
}

parse.listener.on('file', file => {
  file.relPath = file.path.split(project.path + '/')[1];
  file.projectPath = project.path;
  store.object(file);
});

parse.listener.on('directory', data => {
  iter++;
  parse.dirStats(data);
});

parse.listener.on('done', () => {
  iter--;
  if (iter == 0) {
    console.log('Done parsing directory.');
    this.emit('parse-complete', true);
  }
});

module.exports = {
  'clear': clear,
  'parseDir': init,
  'listener': parse.listener
}
