var traceur = require('traceur');
var Emitter = require('events').EventEmitter;

var tableLib = require('./lib/table');
var watch = require('../workers/watcher');

var subEmitter = new Emitter;

var subscribeTo = table => {
  var cTable = tableLib.convert(table);

  return watch.allFrom(cTable)
    .then(cursor => {
      return cursor.toArray((err, results) => {
        return results;
      });
    });
}

module.exports = {
  'subTo': subscribeTo
}
