var conversion = {
  'project': 'project'
}

var convert = table => {
  return conversion[table];
}

module.exports = {
  'convert': convert
}
