var stats = require('../workers/stats')

var processFiles = files => {
  return stats.files(files);
}

module.exports = {
  'processFiles': processFiles
}
