// TODO: Refactor routing to use a separate module for logic, or bake the logic into handlers.
var express = require('express');
var fs = require('fs');
var router = express.Router();
var when = require('when');

var storage = require('../handlers/storage');
var subscriber = require('../handlers/subscribe');
var stats = require('../handlers/stats');

/**
 * A wrapper to render the main page.
 * @param  {JSON Object} req HTTP request
 * @param  {JSON Object} res HTTP response
 * @return {null}
 */
var renderMain = (req, res) => {
  res.render('layout');
}

/**
 * A wrapper to render the main page.
 * @param  {JSON Object} req HTTP request
 * @param  {JSON Object} res HTTP response
 * @return {null}
 */
router.all('/views/:name', (req, res) => {
  var name = req.params.name;
  res.render('../views/' + name);
});

/**
 * Routes to the file parsing routine.
 * @param  {JSON Object} req HTTP request
 * @param  {JSON Object} res HTTP response
 * @return {null}
 */
router.post('/file/parse', (req, res, next) => {
  storage.parseDir(null);
  storage.listener.on('parse-complete', done => {
    if (done) {
      subscriber.subTo('project')
        .then(results => {
          res.end(JSON.stringify(results, null, 2));
        });
    }
  });
});

/**
 * Routes to the subscription routine.
 * @param  {JSON Object} req HTTP request
 * @param  {JSON Object} res HTTP response
 * @return {null}
 */
router.post('/sub/:table', (req, res) => {
  let data = {};

  subscriber.subTo(req.params.table)
    .then(results => {
      data.files = results;
      data.stats = stats.processFiles(data.files);
      res.end(JSON.stringify(data, null, 2));
    });
});

/**
 * Routes to the render main page routine.
 * @param  {JSON Object} req HTTP request
 * @param  {JSON Object} res HTTP response
 * @return {null}
 */
router.all('*', renderMain);

module.exports = router;
