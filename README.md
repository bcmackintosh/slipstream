# Slipstream README #

## What is it? ##

A web app to analyze and visualize a project's software architecture.

## Setup ##

### Platforms ###
It has been developed mostly on Ubuntu 14.04, though it should work on any Linux platform.


### Dependencies ###
Nodejs v12.2.x
RethinkDB

### Installation ###

Clone the slipstream repo to your /opt directory.

Use the following command to run:

```
#!console

$ cd /path/to/slipstream
$ npm install
$ sudo node --harmony --use-strict /path/to/slipstream/app.js
```

## Support ##

**Owner:** Brian Mackintosh bcmackintosh@gmail.com